<?php

namespace VmdCms\Modules\Taxonomies\Collections;

use Illuminate\Contracts\Support\Arrayable;
use VmdCms\CoreCms\Collections\CoreCollectionAbstract;
use VmdCms\Modules\Taxonomies\Contracts\TaxonomyDTOCollectionInterface;
use VmdCms\Modules\Taxonomies\Contracts\TaxonomyDTOInterface;

class TaxonomyDTOCollection extends CoreCollectionAbstract implements TaxonomyDTOCollectionInterface,Arrayable
{
    /**
     * @param TaxonomyDTOInterface $taxonomyDto
     */
    public function append(TaxonomyDTOInterface $taxonomyDto)
    {
        $this->collection->add($taxonomyDto);
    }

    /**
     * @return \Illuminate\Support\Collection
     */
    public function toArray()
    {
        return $this->collection->map(function (TaxonomyDTOInterface $item){
            return $item->toArray();
        });
    }
}
