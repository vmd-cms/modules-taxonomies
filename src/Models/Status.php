<?php

namespace VmdCms\Modules\Taxonomies\Models;

class Status extends Taxonomy
{
   public static function getAnchorKey(): ?string
   {
       return 'status';
   }

    public function getJsonDataFieldsArr() : array
    {
        return ['admin_color'];
    }
}
