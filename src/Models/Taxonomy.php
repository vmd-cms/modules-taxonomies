<?php

namespace VmdCms\Modules\Taxonomies\Models;

use VmdCms\CoreCms\Contracts\Models\ActivableInterface;
use VmdCms\CoreCms\Contracts\Models\CloneableInterface;
use VmdCms\CoreCms\Contracts\Models\CmsModelInterface;
use VmdCms\CoreCms\Contracts\Models\HasInfoInterface;
use VmdCms\CoreCms\Contracts\Models\HasUrlValueInterface;
use VmdCms\CoreCms\Contracts\Models\OrderableInterface;
use VmdCms\CoreCms\Contracts\Models\TreeableInterface;
use VmdCms\CoreCms\Models\CmsModel;
use VmdCms\CoreCms\Traits\Models\Activable;
use VmdCms\CoreCms\Traits\Models\Cloneable;
use VmdCms\CoreCms\Traits\Models\HasImagePath;
use VmdCms\CoreCms\Traits\Models\HasInfo;
use VmdCms\CoreCms\Traits\Models\HasUrl;
use VmdCms\CoreCms\Traits\Models\JsonData;
use VmdCms\CoreCms\Traits\Models\Orderable;
use VmdCms\CoreCms\Traits\Models\Treeable;
use Illuminate\Database\Eloquent\Builder;

class Taxonomy extends CmsModel implements ActivableInterface, HasInfoInterface, TreeableInterface, OrderableInterface, HasUrlValueInterface, CloneableInterface
{
    use Activable,HasInfo,Treeable,Orderable,HasUrl,JsonData,HasImagePath,Cloneable;

    const PARAM_COLOR = 'color';
    const PARAM_IMAGE = 'image';

    public static function table(): string
    {
        return 'taxonomies';
    }

    /**
     * @return null|string
     */
    public static function getAnchorKey() : ?string
    {
        return null;
    }

    protected static function boot()
    {
        parent::boot();

        if(static::getAnchorKey())
        {
            static::addGlobalScope('anchor', function (Builder $builder) {
                $builder->where('key', static::getAnchorKey());
            });
            static::creating(function (Taxonomy $model){
                if(!empty($model->order)) return;
                $lastItem = Taxonomy::where('key',static::getAnchorKey())->orderBy('order','desc')->first();
                if($lastItem instanceof Taxonomy){
                    $model->order = intval($lastItem->order) + 1;
                }
            });
            static::saving(function (Taxonomy $model){
                $model->key = static::getAnchorKey();
            });
        }
    }

    private function getInfoClass() : string
    {
        return TaxonomyInfo::class;
    }

    /**
     * @return string|null
     */
    protected function getImageValue(): ?string
    {
        return $this->param_key == static::PARAM_IMAGE ? $this->param_data : null;
    }

    public function cloneUniqueFields(CmsModelInterface $model): CmsModelInterface
    {
        if(isset($model->url)){
            $model->url = $this->getCopyUniqueValue($model,'url');
        }

        if(isset($model->slug)){
            $model->slug = $this->getCopyUniqueValue($model,'slug');
        }

        return $model;
    }
}
