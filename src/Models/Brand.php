<?php

namespace VmdCms\Modules\Taxonomies\Models;

use App\Modules\Products\Models\Product;
use App\Modules\Products\Models\ProductTaxonomy;

class Brand extends Taxonomy
{
   public static function getAnchorKey(): ?string
   {
       return 'brand';
   }

    public function products(){
        return $this->belongsToMany(Product::class,ProductTaxonomy::table(),'taxonomies_id','products_id');
    }
}
