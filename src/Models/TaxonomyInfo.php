<?php

namespace VmdCms\Modules\Taxonomies\Models;

use VmdCms\CoreCms\Models\CmsInfoModel;

class TaxonomyInfo extends CmsInfoModel
{
    public static function table(): string
    {
        return 'taxonomies_info';
    }
}
