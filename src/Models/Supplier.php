<?php

namespace VmdCms\Modules\Taxonomies\Models;

class Supplier extends Taxonomy
{
   public static function getAnchorKey(): ?string
   {
       return 'supplier';
   }
}
