<?php

namespace VmdCms\Modules\Taxonomies\Models;

class Availability extends Taxonomy
{
   public static function getAnchorKey(): ?string
   {
       return 'availability';
   }
}
