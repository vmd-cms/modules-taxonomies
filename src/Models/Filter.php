<?php

namespace VmdCms\Modules\Taxonomies\Models;

class Filter extends Taxonomy
{
   public static function getAnchorKey(): ?string
   {
       return 'filter';
   }
}
