<?php

namespace VmdCms\Modules\Taxonomies\Traits;

use VmdCms\CoreCms\Exceptions\Models\NotCmsCrossModelException;
use VmdCms\CoreCms\Exceptions\Models\NotCmsModelException;
use VmdCms\Modules\Taxonomies\Models\Supplier as Related;

trait Supplier
{
    use TaxonomyRelated;

    /**
     * @param $value
     * @throws NotCmsModelException
     */
    public function setSupplierAttribute($value)
    {
        $this->getCrossRelatedService()
            ->saveCrossRelation('supplier',$value, $this->getTaxonomyCrossModel());
    }

    /**
     * @return mixed
     * @throws NotCmsCrossModelException
     */
    public function supplier()
    {
        return  $this->getCrossRelatedService()
            ->setCrossModel($this->getTaxonomyCrossModel())
            ->setRelatedModel(Related::class)
            ->hasThroughCrossModelWrapper('hasOneThrough');
    }
}
