<?php

namespace VmdCms\Modules\Taxonomies\Traits;

use VmdCms\CoreCms\Services\Related\CrossRelatedService;

trait TaxonomyRelated
{
    /**
     * @return CrossRelatedService
     */
    protected abstract function getCrossRelatedService(): CrossRelatedService;

    /**
     * @return string
     */
    protected abstract function getTaxonomyCrossModel(): string;
}
