<?php

namespace VmdCms\Modules\Taxonomies\Traits;

use VmdCms\CoreCms\Exceptions\Models\NotCmsCrossModelException;
use VmdCms\CoreCms\Exceptions\Models\NotCmsModelException;
use VmdCms\Modules\Taxonomies\Models\Filter as Related;

trait Filters
{
    use TaxonomyRelated;

    /**
     * @param $value
     * @throws NotCmsModelException
     */
    public function setFiltersAttribute($value)
    {
        $this->getCrossRelatedService()
            ->saveCrossRelation('filters',$value, $this->getTaxonomyCrossModel());
    }

    /**
     * @return mixed
     * @throws NotCmsCrossModelException
     */
    public function filters()
    {
        return  $this->getCrossRelatedService()
            ->setCrossModel($this->getTaxonomyCrossModel())
            ->setRelatedModel(Related::class)
            ->hasThroughCrossModelWrapper('hasManyThrough');
    }
}
