<?php

namespace VmdCms\Modules\Taxonomies\Traits;

use VmdCms\CoreCms\Exceptions\Models\NotCmsCrossModelException;
use VmdCms\CoreCms\Exceptions\Models\NotCmsModelException;
use VmdCms\Modules\Taxonomies\Models\Availability as Related;

trait Availability
{
    use TaxonomyRelated;

    /**
     * @param $value
     * @throws NotCmsModelException
     */
    public function setAvailabilityAttribute($value)
    {
        $this->getCrossRelatedService()
            ->saveCrossRelation('availability',$value, $this->getTaxonomyCrossModel());
    }

    /**
     * @return mixed
     * @throws NotCmsCrossModelException
     */
    public function availability()
    {
        return  $this->getCrossRelatedService()
            ->setCrossModel($this->getTaxonomyCrossModel())
            ->setRelatedModel(Related::class)
            ->hasThroughCrossModelWrapper('hasOneThrough');
    }
}
