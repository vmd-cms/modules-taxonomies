<?php

namespace VmdCms\Modules\Taxonomies\Traits;

use VmdCms\CoreCms\Exceptions\Models\NotCmsCrossModelException;
use VmdCms\CoreCms\Exceptions\Models\NotCmsModelException;
use VmdCms\Modules\Taxonomies\Models\Status as Related;

trait Status
{
    use TaxonomyRelated;

    /**
     * @param $value
     * @throws NotCmsModelException
     */
    public function setStatusAttribute($value)
    {
        $this->getCrossRelatedService()
            ->saveCrossRelation('status',$value,$this->getTaxonomyCrossModel());
    }

    /**
     * @return mixed
     * @throws NotCmsCrossModelException
     */
    public function status()
    {
        return  $this->getCrossRelatedService()
            ->setCrossModel($this->getTaxonomyCrossModel())
            ->setRelatedModel(Related::class)
            ->hasThroughCrossModelWrapper('hasManyThrough');
    }

}
