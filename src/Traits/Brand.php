<?php

namespace VmdCms\Modules\Taxonomies\Traits;

use VmdCms\CoreCms\Exceptions\Models\NotCmsCrossModelException;
use VmdCms\CoreCms\Exceptions\Models\NotCmsModelException;
use VmdCms\Modules\Taxonomies\Models\Brand as Related;

trait Brand
{
    use TaxonomyRelated;

    /**
     * @param $value
     * @throws NotCmsModelException
     */
    public function setBrandAttribute($value)
    {
        $this->getCrossRelatedService()
            ->saveCrossRelation('brand',$value, $this->getTaxonomyCrossModel());
    }

    /**
     * @return mixed
     * @throws NotCmsCrossModelException
     */
    public function brand()
    {
        return  $this->getCrossRelatedService()
            ->setCrossModel($this->getTaxonomyCrossModel())
            ->setRelatedModel(Related::class)
            ->hasThroughCrossModelWrapper('hasOneThrough');
    }
}
