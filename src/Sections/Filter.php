<?php

namespace VmdCms\Modules\Taxonomies\Sections;

use Illuminate\Database\Eloquent\Builder;
use VmdCms\CoreCms\Collections\DependedComponentCollection;
use VmdCms\CoreCms\Contracts\Dashboard\Display\DisplayInterface;
use VmdCms\CoreCms\Contracts\Dashboard\Forms\FormInterface;
use VmdCms\CoreCms\CoreModules\CoreTranslates\Services\CoreLang;
use VmdCms\CoreCms\Dashboard\Display\Filters\FilterDisplay;
use VmdCms\CoreCms\Dashboard\Display\Filters\FilterTreeComponent;
use VmdCms\CoreCms\Dashboard\Forms\Components\DependedComponent;
use VmdCms\CoreCms\Dashboard\Forms\Components\InputComponent;
use VmdCms\CoreCms\DTO\Dashboard\DependComponentDto;
use VmdCms\CoreCms\Facades\Column;
use VmdCms\CoreCms\Facades\ColumnEditable;
use VmdCms\CoreCms\Facades\Display;
use VmdCms\CoreCms\Facades\Form;
use VmdCms\CoreCms\Facades\FormComponent;
use VmdCms\Modules\Taxonomies\Enums\TaxonomyParamEnum;

class Filter extends Taxonomy
{
    /**
     * @var string
     */
    protected $slug = 'filters';

    /**
     * @inheritDoc
     */
    public function getTitle() : string
    {
        return CoreLang::get('filters');
    }

    public function getCmsModelClass(): string
    {
        return \VmdCms\Modules\Taxonomies\Models\Filter::class;
    }

    /**
     * @return DisplayInterface
     */
    public function display()
    {
        $filter = (new FilterDisplay())
            ->appendFilterComponent((new FilterTreeComponent('info.title'))
                ->setHeadTitle($this->getTitle())
                ->setFilterField('parent_id')
                ->setModelClass($this->getCmsModelClass())
            );
        return Display::dataTable([
            Column::text('id','#')->searchable(),
            ColumnEditable::text('info.title',CoreLang::get('title'))->maxLength(30)
                ->setSearchableCallback(function ($query, $search) {
                    $query->orWhereHas('info', function ($q) use ($search) {
                        $q->where('title', 'like', '%' . $search . '%');
                    });
                }),
            ColumnEditable::text('url','URL')->searchable()->sortable()->unique(),
            ColumnEditable::text('slug','Slug')->searchable()->maxLength(15),
            ColumnEditable::switch('active')->alignCenter(),
        ])->orderDefault(function ($query){
            $query->orderBy('created_at','desc');
        })->whereNull('parent_id')->setSearchable(true)->setFilter($filter)->orderable();
    }

    /**
     * @param int|null $id
     * @return FormInterface
     */
    public function edit(?int $id) : FormInterface
    {
        $dependedCollection = new DependedComponentCollection();
        $dependedCollection->appendItem(new DependComponentDto(TaxonomyParamEnum::PARAM_COLOR, FormComponent::colorPicker('')->setLabel('Выберите цвет')));
        $dependedCollection->appendItem(new DependComponentDto(TaxonomyParamEnum::PARAM_IMAGE, FormComponent::image('')->setLabel('Выберите картиинку')));
        $dependedComponent = new DependedComponent('param_data',$dependedCollection);

        return Form::panel([
            FormComponent::input('info.title',CoreLang::get('title'))->required(),
            FormComponent::url('url','URL')->setDependedField('info.title')->unique(),
            FormComponent::dependedSelect('param_key',CoreLang::get('type_filter'))
                ->setEnumValues(TaxonomyParamEnum::params())
                ->setDependedComponent($dependedComponent),
            FormComponent::select('parent_id',CoreLang::get('group'))
                ->setModelForOptions(\VmdCms\Modules\Taxonomies\Models\Filter::class)
                ->setDisplayField('info.title')
                ->setForeignField('id')
                ->setWhere(['parent_id','=',null])
                ->nullable(),
            FormComponent::radio('active')
        ])->setHeadTitle(CoreLang::get('editing_filter'), 'info.title');
    }

}
