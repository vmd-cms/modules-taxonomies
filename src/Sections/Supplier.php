<?php

namespace VmdCms\Modules\Taxonomies\Sections;

use VmdCms\CoreCms\CoreModules\CoreTranslates\Services\CoreLang;

class Supplier extends Taxonomy
{
    /**
     * @var string
     */
    protected $slug = 'supplier';

    /**
     * @inheritDoc
     */
    public function getTitle() : string
    {
        return CoreLang::get('supplier');
    }

    public function getCmsModelClass(): string
    {
        return \App\Modules\Taxonomies\Models\Supplier::class;
    }

}
