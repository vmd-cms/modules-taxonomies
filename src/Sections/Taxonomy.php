<?php

namespace VmdCms\Modules\Taxonomies\Sections;

use VmdCms\CoreCms\Contracts\Dashboard\Display\DisplayInterface;
use VmdCms\CoreCms\Contracts\Dashboard\Forms\FormInterface;
use VmdCms\CoreCms\CoreModules\CoreTranslates\Services\CoreLang;
use VmdCms\CoreCms\Facades\Column;
use VmdCms\CoreCms\Facades\ColumnEditable;
use VmdCms\CoreCms\Facades\Display;
use VmdCms\CoreCms\Facades\Form;
use VmdCms\CoreCms\Facades\FormComponent;
use VmdCms\CoreCms\Models\CmsSection;

abstract class Taxonomy extends CmsSection
{
    public function display()
    {
        return Display::dataTable([
            Column::text('id','#')->searchable(),
            ColumnEditable::text('info.title',CoreLang::get('title'))
                ->setSearchableCallback(function ($query, $search) {
                    $query->orWhereHas('info', function ($q) use ($search) {
                        $q->where('title', 'like', '%' . $search . '%');
                    });
                }),
            ColumnEditable::text('url')->sortable()->searchable()->unique(),
            ColumnEditable::switch('active')->sortable()->alignCenter(),
            Column::date('created_at')->setFormat('Y-m-d'),
        ])->orderDefault(function ($query){
            $query->orderBy('created_at','desc');
        })->setSearchable(true);
    }

    /**
     * @return FormInterface
     */
    public function create() : FormInterface
    {
        return $this->edit(null);
    }

    /**
     * @param int|null $id
     * @return FormInterface
     */
    public function edit(?int $id) : FormInterface
    {
        return Form::panel([
            FormComponent::input('info.title',CoreLang::get('title'))->required(),
            FormComponent::url('url','Url')->setDependedField('info.title')->unique(),
            FormComponent::switch('active')
        ]);
    }

}
