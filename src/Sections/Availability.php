<?php

namespace VmdCms\Modules\Taxonomies\Sections;

use VmdCms\CoreCms\Contracts\Dashboard\Forms\FormInterface;
use VmdCms\CoreCms\CoreModules\CoreTranslates\Services\CoreLang;
use VmdCms\CoreCms\CoreModules\Moderators\Entity\AuthEntity;
use VmdCms\CoreCms\Facades\Form;
use VmdCms\CoreCms\Facades\FormComponent;

class Availability extends Taxonomy
{
    /**
     * @var string
     */
    protected $slug = 'availability';

    /**
     * @inheritDoc
     */
    public function getTitle() : string
    {
        return CoreLang::get('availability');
    }

    public function getCmsModelClass(): string
    {
        return \App\Modules\Taxonomies\Models\Availability::class;
    }

    /**
     * @param int|null $id
     * @return FormInterface
     */
    public function edit(?int $id) : FormInterface
    {
        $components = [
            FormComponent::input('info.title',CoreLang::get('title'))->required(),
            FormComponent::url('url','Url')->setDependedField('info.title')->unique(),
            FormComponent::switch('active')
        ];
        if(AuthEntity::getAuthModerator()->has_full_access){
            $components[] = FormComponent::url('slug','Slug')->setDependedField('info.title')->unique();
        }
        return Form::panel($components);
    }

}
