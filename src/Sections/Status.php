<?php

namespace VmdCms\Modules\Taxonomies\Sections;

use VmdCms\CoreCms\Contracts\Dashboard\Forms\FormInterface;
use VmdCms\CoreCms\CoreModules\CoreTranslates\Services\CoreLang;
use VmdCms\CoreCms\CoreModules\Moderators\Entity\AuthEntity;
use VmdCms\CoreCms\Facades\Column;
use VmdCms\CoreCms\Facades\ColumnEditable;
use VmdCms\CoreCms\Facades\Display;
use VmdCms\CoreCms\Facades\Form;
use VmdCms\CoreCms\Facades\FormComponent;

class Status extends Taxonomy
{
    /**
     * @var string
     */
    protected $slug = 'status';

    public function display()
    {
        return Display::dataTable([
            Column::text('id','#')->searchable(),
            ColumnEditable::text('info.title',CoreLang::get('title'))->maxLength(30)
                ->setSearchableCallback(function ($query, $search) {
                    $query->orWhereHas('info', function ($q) use ($search) {
                        $q->where('title', 'like', '%' . $search . '%');
                    });
                }),
            ColumnEditable::colorPicker('admin_color',CoreLang::get('color')),
            ColumnEditable::switch('active')->alignCenter(),
        ])->orderDefault(function ($query){
            $query->orderBy('created_at','desc');
        })->setSearchable(true);
    }

    /**
     * @inheritDoc
     */
    public function getTitle() : string
    {
        return CoreLang::get('status');
    }

    public function getCmsModelClass(): string
    {
        return \App\Modules\Taxonomies\Models\Status::class;
    }

    /**
     * @param int|null $id
     * @return FormInterface
     */
    public function edit(?int $id) : FormInterface
    {
        $components = [
            FormComponent::input('info.title',CoreLang::get('title'))->required(),
            FormComponent::url('url','Url')->setDependedField('info.title')->unique(),
            FormComponent::switch('active')
        ];
        if(AuthEntity::getAuthModerator()->has_full_access){
            $components[] = FormComponent::url('slug','Slug')->setDependedField('info.title')->unique();
        }
        return Form::panel($components);
    }

}
