<?php

namespace VmdCms\Modules\Taxonomies\Sections;

use VmdCms\CoreCms\Contracts\Dashboard\Forms\FormInterface;
use VmdCms\CoreCms\CoreModules\CoreTranslates\Services\CoreLang;
use VmdCms\CoreCms\Facades\Form;
use VmdCms\CoreCms\Facades\FormComponent;

class Brand extends Taxonomy
{
    /**
     * @var string
     */
    protected $slug = 'brand';

    /**
     * @inheritDoc
     */
    public function getTitle() : string
    {
        return CoreLang::get('brand');
    }

    public function getCmsModelClass(): string
    {
        return \App\Modules\Taxonomies\Models\Brand::class;
    }
}
