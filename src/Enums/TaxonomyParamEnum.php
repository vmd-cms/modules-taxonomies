<?php

namespace VmdCms\Modules\Taxonomies\Enums;

class TaxonomyParamEnum
{
    const PARAM_COLOR = 'color';
    const PARAM_IMAGE = 'image';
    const PARAM_DEFAULT = 'default';

    public static function params()
    {
        return [
            self::PARAM_DEFAULT => 'Обычный',
            self::PARAM_COLOR => 'Цвет',
            self::PARAM_IMAGE => 'Рисунок',
        ];
    }
}
