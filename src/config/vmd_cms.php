<?php

return [
    "taxonomies_module" => [
        "prefix_list" => "taxonomies",
        "prefix_single" => "taxonomy",
    ]
];
