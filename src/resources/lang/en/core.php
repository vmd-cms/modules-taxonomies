<?php

return [
    /**
     * fields
     **/
    'type_filter' => "Filter Type",
    'editing_filter' => "Editing Filter:",

    /**
     * pages
     */
    'taxonomies' => 'Taxonomies',
    'filter' => 'Filter',
    'filters' => 'Filters',
    'brand' => 'Brand',
    'supplier' => 'Supplier',
    'status' => 'Status',
    'availability' => 'Availability',

    /**
     * messages
     **/

    /**
     * buttons
     **/

    /**
     * validates
     */
];
