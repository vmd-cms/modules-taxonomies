<?php

return [
    /**
     * fields
     **/
    'type_filter' => "Тип фильтра",
    'editing_filter' => "Редактирование фильтра:",

    /**
     * pages
     */
    'taxonomies' => 'Таксономии',
    'filter' => 'Фильтр',
    'filters' => 'Фильтры',
    'brand' => 'Бренд',
    'supplier' => 'Поставщик',
    'status' => 'Статус',
    'availability' => 'Доступность',

    /**
     * messages
     **/

    /**
     * buttons
     **/

    /**
     * validates
     */
];
