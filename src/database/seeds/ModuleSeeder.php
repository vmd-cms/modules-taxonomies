<?php

namespace VmdCms\Modules\Taxonomies\database\seeds;

use Illuminate\Support\Str;
use VmdCms\CoreCms\CoreModules\CoreTranslates\Services\CoreLang;
use VmdCms\CoreCms\Initializers\AbstractModuleSeeder;
use App\Modules\Taxonomies\Sections\Availability;
use App\Modules\Taxonomies\Sections\Status;
use App\Modules\Taxonomies\Sections\Supplier;
use App\Modules\Taxonomies\Models\TaxonomyInfo;
use App\Modules\Taxonomies\Sections\Filter;
use App\Modules\Taxonomies\Sections\Brand;
use VmdCms\Modules\Taxonomies\Models\Taxonomy;

class ModuleSeeder extends AbstractModuleSeeder
{

    /**
     * @inheritDoc
     */
    protected function getSectionsAssoc(): array
    {
        return [
            Availability::class => CoreLang::get('filter') . ' ' . CoreLang::get('availability'),
            Status::class =>  CoreLang::get('filter') . ' ' . CoreLang::get('status'),
            Supplier::class =>  CoreLang::get('filter') . ' ' . CoreLang::get('supplier'),
            Filter::class =>  CoreLang::get('filters'),
            Brand::class => CoreLang::get('brand'),
        ];
    }

    /**
     * @inheritDoc
     */
    protected function getNavigationAssoc(): array
    {
        return [
            [
                "slug" => "shop_group",
                "title" => CoreLang::get('shop'),
                "is_group_title" => true,
                "order" => 2,
                "children" => [
                    [
                        "slug" => "taxonomies",
                        "title" => CoreLang::get('taxonomies'),
                        "icon" => "icon icon-filter",
                        "children" => [
                            [
                                "slug" => "filters",
                                "title" => CoreLang::get('taxonomies'),
                                "section_class" => Filter::class
                            ],
                            [
                                "slug" => "availability",
                                "title" => CoreLang::get('availability'),
                                "section_class" => Availability::class
                            ],
                            [
                                "slug" => "status",
                                "title" => CoreLang::get('status'),
                                "section_class" => Status::class
                            ],
                            [
                                "slug" => "brand",
                                "title" => CoreLang::get('brand'),
                                "section_class" => Brand::class
                            ]
                        ]
                    ]
                ]
            ]
        ];
    }

    protected function seedModelData()
    {
        try {
            $arrPath = base_path('database/seeds/modules/taxonomies_arr.php');
            if(!file_exists($arrPath)) throw new \Exception();
            $arr = include $arrPath;
            if(!is_array($arr) || !count($arr)) throw new \Exception();
            foreach ($arr as $key=>$items)
            {
                if(!is_array($items) || !count($items)) continue;
                foreach ($items as $item)
                {
                    $this->seedTaxItem($key,$item);
                }
            }
        }
        catch (\Exception $exception){

        }
    }

    private function seedTaxItem($key, $item, $parentId = null)
    {
        try {
            $model = new Taxonomy();
            $model->key = $key;
            $model->slug = $item['slug'] ?? null;
            $model->parent_id = $parentId;
            $model->active = $item['active'];
            $model->url = $item['url'] ?? Str::slug($item['title']);
            $model->save();
        }
        catch (\Exception $exception){
            $model->url = $item['url'] ?? Str::slug($item['title']) . rand(12345,98765);
            $model->save();
        }

        $modelInfo = new \VmdCms\Modules\Taxonomies\Models\TaxonomyInfo();
        $modelInfo->taxonomies_id = $model->id;
        $modelInfo->title = $item['title'];
        $modelInfo->url =  $item['url'] ?? Str::slug($item['title']);
        $modelInfo->save();

        if(isset($item['children']) && is_countable($item['children']) && count($item['children'])){
            foreach ($item['children'] as $child)
            {
                $this->seedTaxItem($key,$child,$model->id);
            }
        }
        return;
    }
}
