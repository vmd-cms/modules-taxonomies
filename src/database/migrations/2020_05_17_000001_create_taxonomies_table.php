<?php

use VmdCms\Modules\Taxonomies\Models\Taxonomy as model;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTaxonomiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(model::table(), function (Blueprint $table) {
            $table->increments(model::getPrimaryField());
            $table->string('key',32);
            $table->integer('parent_id')->unsigned()->nullable();
            $table->string('url',128)->nullable();
            $table->string('slug',32)->nullable();
            $table->string('param_key',32)->nullable()->default('default');
            $table->string('param_data',512)->nullable();
            $table->text('data')->nullable();
            $table->boolean('active')->default(false);
            $table->integer('order')->unsigned()->default(1);
            $table->string('import_source_id',128)->nullable();
            $table->timestamps();
            $table->unique(['key','parent_id', 'url']);
            $table->unique(['key','parent_id', 'slug']);
        });

        Schema::table(model::table(), function (Blueprint $table){
            $table->foreign('parent_id', model::table() . '_parent_id_fk')
                ->references('id')->on(model::table())
                ->onUpdate('CASCADE')->onDelete('SET NULL');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists(model::table());
    }
}
