<?php

namespace VmdCms\Modules\Taxonomies\Contracts;

use VmdCms\CoreCms\Contracts\Collections\CollectionInterface;

interface TaxonomyDTOCollectionInterface extends CollectionInterface
{
    /**
     * @param TaxonomyDTOInterface $catalogEntity
     */
    public function append(TaxonomyDTOInterface $catalogEntity);

}
