<?php

namespace VmdCms\Modules\Taxonomies\Contracts;

use Illuminate\Contracts\Support\Arrayable;
use VmdCms\Modules\Taxonomies\Models\Taxonomy;

interface TaxonomyDTOInterface extends Arrayable
{
    /**
     * TaxonomyDTOInterface constructor.
     * @param Taxonomy $taxonomy
     * @param bool $recursive
     * @param bool $fullUrlPath
     */
    public function __construct(Taxonomy $taxonomy, bool $recursive = true, bool $fullUrlPath = false);

    /**
     * @return string|null
     */
    public function getTitle(): ?string;

    /**
     * @return string|null
     */
    public function getImagePath(): ?string;

    /**
     * @return string|null
     */
    public function getUrl(): ?string;

    /**
     * @return string|null
     */
    public function getSlug(): ?string;

    /**
     * @return string|null
     */
    public function getKey(): ?string;

    /**
     * @return int|null
     */
    public function getCount(): ?int;

    /**
     * @param int $count
     * @return TaxonomyDTOInterface
     */
    public function setCount(int $count): TaxonomyDTOInterface;

    /**
     * @param TaxonomyDTOCollectionInterface $collection
     * @return TaxonomyDTOInterface
     */
    public function setChildren(TaxonomyDTOCollectionInterface $collection): TaxonomyDTOInterface;

    /**
     * @return TaxonomyDTOCollectionInterface|null
     */
    public function getChildren(): ?TaxonomyDTOCollectionInterface;

}
