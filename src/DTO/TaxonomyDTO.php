<?php

namespace VmdCms\Modules\Taxonomies\DTO;

use VmdCms\CoreCms\Contracts\Models\TreeableInterface;
use VmdCms\Modules\Taxonomies\Collections\TaxonomyDTOCollection;
use VmdCms\Modules\Taxonomies\Contracts\TaxonomyDTOCollectionInterface;
use VmdCms\Modules\Taxonomies\Contracts\TaxonomyDTOInterface;
use VmdCms\Modules\Taxonomies\Models\Taxonomy;

class TaxonomyDTO implements TaxonomyDTOInterface
{
    public $id;
    public $parentId;
    public $title;
    public $imagePath;
    public $url;
    public $slug;
    public $children;
    public $parent;
    public $key;
    public $count;

    public function __construct(Taxonomy $taxonomy,bool $recursive = true, bool $withParent = false)
    {
        $this->id = $taxonomy->id;
        $this->parentId = $taxonomy->parent_id;
        $this->title = $taxonomy->info->title ?? null;
        $this->imagePath = $taxonomy->imagePath ?? null;
        $this->url = $taxonomy->urlValue;
        $this->slug = $taxonomy->slug;
        $this->key = $taxonomy->key;
        $this->count = 0;
        if($recursive && $taxonomy instanceof TreeableInterface && count($taxonomy->children))
        {
            $this->children = new TaxonomyDTOCollection();
            foreach ($taxonomy->children as $item)
            {
                $this->children->append(new \App\Modules\Taxonomies\DTO\TaxonomyDTO($item, $recursive, $withParent));
            }
        }
        if($withParent && $this->parentId)
        {
            $this->parent = new \App\Modules\Taxonomies\DTO\TaxonomyDTO($taxonomy->parent, $recursive, $withParent);
        }
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return int
     */
    public function getParentId(): int
    {
        return $this->parentId;
    }

    /**
     * @return string|null
     */
    public function getTitle(): ?string
    {
        return $this->title;
    }

    /**
     * @return string|null
     */
    public function getImagePath(): ?string
    {
        return $this->imagePath;
    }

    /**
     * @return string|null
     */
    public function getUrl(): ?string
    {
        return $this->url;
    }

    /**
     * @param TaxonomyDTOCollectionInterface $collection
     * @return TaxonomyDTOInterface
     */
    public function setChildren(TaxonomyDTOCollectionInterface $collection): TaxonomyDTOInterface
    {
        $this->children = $collection;
        return $this;
    }

    /**
     * @return TaxonomyDTOCollectionInterface|null
     */
    public function getChildren(): ?TaxonomyDTOCollectionInterface
    {
        return $this->children;
    }

    /**
     * @return null|\App\Modules\Taxonomies\DTO\TaxonomyDTO
     */
    public function getParent(): ?\App\Modules\Taxonomies\DTO\TaxonomyDTO
    {
        return $this->parent;
    }

    public function getKey(): ?string
    {
        return $this->key;
    }

    public function getCount(): ?int
    {
        return $this->count;
    }

    public function setCount(int $count): TaxonomyDTOInterface
    {
        $this->count = $count;
        return $this;
    }

    public function getSlug(): ?string
    {
        return $this->slug;
    }

    public function getSubItem(string $slug = null): ?TaxonomyDTO
    {
        $subItem = null;

        if($this->children instanceof TaxonomyDTOCollection) {
            $subItem = empty($slug) ? $this->children->getItems()->first() : null;
        }

        return $subItem;
    }

    public function getSubItemTitle(string $slug = null): ?string
    {
        $subItem = $this->getSubItem($slug);
        return $subItem instanceof TaxonomyDTO ? $subItem->getTitle() : null;
    }

    public function toArray()
    {
        return [
            'id' => $this->id,
            'parentId' => $this->parentId,
            'title' => $this->title,
            'imagePath' => $this->imagePath,
            'url' => $this->url,
            'slug' => $this->slug,
            'children' => $this->children,
            'parent' => $this->parent,
            'key' => $this->key,
            'count' => $this->count,
        ];
    }
}
